const ageInput = document.querySelector('input[name=age]');
const relationshipInput = document.querySelector('select[name=rel]');
const smokerInput = document.querySelector('input[name=smoker]');
const addButton = document.querySelector('button.add');
const submitButton = document.querySelector('button[type=submit]');
const formElement = document.querySelector('form');
const debugElement = document.querySelector('pre.debug');
const householdElement = document.querySelector('ol.household');
const announcementRegion = document.createElement('div');
const styles = document.querySelectorAll('style')[0];

let household = [];
const emptyMessage = 'Please add at least one member to your family.';
const pageStyles = `
  h1 {
    text-align: center;
  }
  .builder {
    display: flex;
    margin: 0 auto 2rem;
    max-width: 60rem;
    font-family: sans-serif;
  }
  .household {
    border: 1px solid grey;
    flex-basis: 50%;
    order: 2;
    margin: 0;
    padding: 0;
    max-height: calc(100vh - 10rem);
    overflow-y: scroll;
  }
  .household .member {
    padding: 1rem;
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid lightgrey;
  }
  .household .member + .member {
    border-top: 1px solid #ccc;
  }
  .household .name {
    font-weight: bold;
  }
  .household .relationship {
    font-style: italic;
    font-size: .8rem;
  }
  .household .smoker {
    display: inline-block;
    margin: auto .25rem;
    font-size: .4rem;
    width: .8rem;
    height: .8rem;
    line-height: .9rem;
    vertical-align: text-top;
    text-align: center;
    color: #777;
    border: 1px solid #aaa;
    border-radius: 100%;
    font-weight: bold;
    text-transform: uppercase;
  }
  .household .help {
    padding: 1rem;
    font-style: italic;
  }
  form {
    flex-basis: 50%;
    display: flex;
    flex-flow: column wrap;
    padding; .5rem 0;
  }
  form > div {
    margin: .25rem 0;
  }
  .debug {
    overflow: scroll;
  }
  .sr-only {
    position: absolute;
    left: -999em;
    right: auto;
  }
`;

function fixFormDOM () {
  // Add a Name Field
  let nameSection = document.createElement('div');
  let nameLabel = document.createElement('label');
  let nameInput = document.createElement('input');
  nameInput.name = 'name';
  nameInput.type = 'text';
  nameInput.required = true;
  nameInput.setAttribute('id', 'firstFocus')
  nameLabel.append('Name ', nameInput);
  nameSection.append(nameLabel);
  formElement.prepend(nameSection);


  // Set basic validation
  relationshipInput.required = true;
  ageInput.required = true;
  ageInput.min = 0;
  ageInput.type = 'number'


  // Improve starting form element attributes & language
  addButton.innerHTML = 'Add Family Member';
  submitButton.id = 'submitFamily';
  submitButton.innerHTML = 'Submit Family';
  smokerInput.checked = false;
  smokerInput.value = null;
  submitButton.disabled = true;
  householdElement.innerHTML = `<div class='help'>${emptyMessage}</div>`;

  // add tabindexes & labels for keyboard navigators
  nameInput.setAttribute('tabindex', 1);
  nameInput.setAttribute('aria-label', 'First name of the household member');

  ageInput.setAttribute('tabindex', 2);
  ageInput.setAttribute('aria-label', 'Age of the household member, in years');

  relationshipInput.setAttribute('tabindex', 3);
  relationshipInput.setAttribute('aria-label', 'Relationship to you of the household member');

  smokerInput.setAttribute('tabindex', 4);
  smokerInput.setAttribute('aria-label', 'Is the household member a smoker');

  addButton.setAttribute('tabindex', 5);
  addButton.setAttribute('aria-label', 'Add this member to your household');

  submitButton.setAttribute('tabindex', 6);
  submitButton.setAttribute('aria-label', 'Submit your complete household');

  formElement.setAttribute('aria-label', 'Household information');
  
  householdElement.setAttribute('tabindex', 9);
  householdElement.setAttribute('aria-label', 'Household members');


  // add an aria-live region to announce changes to the list
  announcementRegion.setAttribute('aria-live', 'polite')
  announcementRegion.classList.add('sr-only');
  announcementRegion.classList.add('announcement-region');
  householdElement.insertAdjacentElement('afterend', announcementRegion);
}

function updateHouseholdDOM(household = []) {

  if (household.length > 0) {

    householdElement.innerHTML = household.map( (member, index) => {
      return `<li class="member" id="${member.guid}" tabindex="${10 + index}" 
                aria-label="${member.name}, age ${member.age}, ${member.relationship}, is ${!member.isSmoker?'not':''} a smoker">
                <div>
                  <span class="name">
                    ${member.name}
                  </span>
                  ${member.isSmoker ? '<span class="smoker">S</span>' : ''}
                  <div class="relationship">
                    ${member.relationship},
                    <span class="age">
                      ${member.age}
                    </span>
                  </div>
                </div>
                <button tabindex="${10 + index}" aria-label="Remove this member from your household" onClick="removeMember('${member.guid}')">Remove</button>
              </li>`;
    }).join('');

    announcementRegion.innerText = `Household members updated. Household now contains ${household.map(member=>member.name).join(', ')}.`;
    submitButton.disabled = false;
  } else {
    householdElement.innerHTML = '';
    announcementRegion.innerText = `Household members updated. Household is now empty.`;
    submitButton.disabled = true;
    householdElement.innerHTML = `<div class='help'>${emptyMessage}</div>`;
  }
}

function addMemberToHousehold(memberFormData) {
  try {
    const personData = {
      name: sanitizeInput(memberFormData.get('name')),
      age: parseInt(memberFormData.get('age')),
      relationship: memberFormData.get('rel'),
      isSmoker: memberFormData.has('smoker'),
      guid: 'id-' + Date.now() + Math.floor(Math.random() * 100) // lazy guid
    };
    household.push(personData); 
    updateHouseholdDOM(household);
  } catch {
   throw new Error("invalid member parameters passed.")
  }
};

function removeMember(guid) {
  household = household.filter(member => member.guid !== guid)
  updateHouseholdDOM(household);
}

function sanitizeInput(string) {
var map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      "/": '&#x2F;',
      "\\": '&#x5c;'
  };
  var regex = /[&<>"'/]/ig;
  return string.replace(regex, (match)=>(map[match]));
}

function focusFirst() {
  document.getElementById('firstFocus').focus();
}

function displayDebug() {
  let submissionData = household.map(member => Object.assign({}, member));
  submissionData.forEach(member => { delete member.guid })
  debugElement.innerHTML = JSON.stringify(submissionData);
  debugElement.style.display = 'block';
}

function clearDebug() {
  debugElement.innerHTML = '';
  debugElement.style.display = 'none';
}

window.addEventListener('DOMContentLoaded', () => {
  fixFormDOM();
  styles.append(pageStyles);

  formElement.addEventListener('submit', (e) => {
    e.preventDefault();
    new FormData(formElement);
    formElement.reset();
  });

  formElement.addEventListener('formdata', (event) => {
    addMemberToHousehold(event.formData);
  });

  submitButton.addEventListener('click', () => {
    event.preventDefault();
    displayDebug();
  });
  
  focusFirst();
});

