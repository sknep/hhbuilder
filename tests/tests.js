describe("Builder", function() {
  const firstMember = {
    guid: "id-stub-new-member",
    name: "New Member",
    age: 99,
    relationship: "self",
    isSmoker: false
  };

  const secondMember = {
    guid: "id-stub-new-member-2",
    name: "New Member 2",
    age: 2,
    relationship: "child",
    isSmoker: false
  };

  let stubMember = new FormData();
  stubMember.append("name", "Newest Member");
  stubMember.append("age", "1");
  stubMember.append("rel", "other");

  afterEach(function () {
    // reset household
    household = [];
    updateHouseholdDOM([]);
    clearDebug();
  });

  describe("Initial state", function() {  

    it("should start with an empty household", function() {
      expect(household).toEqual([])
    });
    
  });

  describe("Setup Form DOM", function() {  

    it("should add a name input to the DOM and focus it", function() {
      const foundNameInput = document.querySelector('input[name="name"]');
      const foundFirstFocus = document.querySelector('#firstFocus');
      const focusedElement = document.activeElement;
      expect(foundNameInput).toBeTruthy();
      expect(foundFirstFocus).toEqual(foundNameInput);
      expect(foundFirstFocus).toEqual(focusedElement);
    });

    it("should make age a required number and at least 0", function() {
      const foundAgeInput = document.querySelector('input[name="age"]');
      expect(foundAgeInput.hasAttribute("required")).toBeTruthy();
      expect(foundAgeInput.getAttribute("type")).toBe("number");
      expect(foundAgeInput.getAttribute("min")).toBe('0');
    });

    it("should improve button initial state & text", function() {
      const foundAddButton = document.querySelector('button.add');
      const foundSubmitButton = document.querySelector('button[type="submit"]');
      expect(foundAddButton.innerText).toBe('Add Family Member');
      expect(foundSubmitButton.innerText).toBe('Submit Family');
      expect(foundSubmitButton.hasAttribute('disabled')).toBeTruthy();
    });

    it("should add an aria-label and tabindex to all inputs, select areas, and buttons", function() {
      const allInputs = [
        ...document.querySelectorAll('input'),
        ...document.querySelectorAll('select'),
        ...document.querySelectorAll('button'),
      ];
      const unlabelledInputs = allInputs.filter(input => input.ariaLabel === null)
      const unIndexedInputs = allInputs.filter(input => input.tabIndex < 1)
      expect(unlabelledInputs).toEqual([])
      expect(unIndexedInputs).toEqual([])
    });

    it("should add an aria-live region", function() {
      const foundAnnouncementRegion = document.querySelector('div[aria-live=polite]');
      expect(foundAnnouncementRegion).toBeTruthy();
    });


  });

  describe("Update household DOM", function () {
    let foundHouseholdList;
    let foundSubmitHouseholdButton;
    let foundAriaLiveRegion;

    beforeEach(function () {
      foundHouseholdList = document.querySelector('.household');
      foundSubmitHouseholdButton = document.querySelector('button[type=submit]');
      foundAriaLiveRegion = document.querySelector('div.announcement-region');

    })

    it("should display and announce a household with one member", function() {
      updateHouseholdDOM( [firstMember] );
      expect(foundHouseholdList.querySelectorAll('li').length).toEqual(1);
      expect(foundHouseholdList.querySelectorAll('li')[0].getAttribute('id')).toEqual(firstMember.guid);
      expect(foundSubmitHouseholdButton.hasAttribute('disabled')).toBeFalsy();
      expect(foundAriaLiveRegion.innerText).toBe(`Household members updated. Household now contains ${firstMember.name}.`)
    });

    it("should display and announce a household with two members", function() {
      updateHouseholdDOM( [firstMember, secondMember] );
      expect(foundHouseholdList.querySelectorAll('li').length).toEqual(2);
      expect(foundHouseholdList.querySelectorAll('li')[1].getAttribute('id')).toEqual(secondMember.guid);
      expect(foundSubmitHouseholdButton.hasAttribute('disabled')).toBeFalsy();
      expect(foundAriaLiveRegion.innerText).toBe(`Household members updated. Household now contains ${firstMember.name}, ${secondMember.name}.`)
    });

    it("should display and announce a household with no members", function() {
      updateHouseholdDOM( [] );
      expect(foundHouseholdList.querySelectorAll('li').length).toEqual(0);
      expect(foundHouseholdList.querySelectorAll('div.help').length).toEqual(1);
      expect(foundSubmitHouseholdButton.hasAttribute('disabled')).toBeTruthy();
      expect(foundAriaLiveRegion.innerText).toBe('Household members updated. Household is now empty.')
    });

  });
 
  describe("Manage household", function () {

    it("should add a valid member to the household", function () {
      addMemberToHousehold(stubMember);
      expect(household.length).toBe(1);
    });

    it("should remove a member from the household", function (done) {
      addMemberToHousehold(stubMember);
      expect(household.length).toBe(1);
      setTimeout(function() {
        const memberGuidToRemove = household[0].guid;
        removeMember(memberGuidToRemove);
        expect(household.length).toBe(0);
        done();
      }, 100);
    });

    it("should throw an error if asked to add an invalid member", function () {
      let badMember = new FormData();
      stubMember.append("name", null);
      stubMember.append("age", "-1");
      stubMember.append("rel", "foo");
      expect(() => addMemberToHousehold(badMember))
        .toThrow(new Error("invalid member parameters passed."));
      expect(household.length).not.toBe(1);
    });

  });

  describe("Debug", function () {

    it("should append the current household to the page in a debug container", function() {
      const foundDebug = document.querySelector('pre.debug');

      displayDebug();
      expect(foundDebug.style.display).toBe('block');
      expect(foundDebug.innerHTML).toContain('[]');

      addMemberToHousehold(stubMember);

      displayDebug();
      expect(foundDebug.innerHTML).not.toContain('[]');

    });
  });

  describe("Form behavior", function () {

    it("should sanitize user input", function () {
      const naughtyStrings = [
        {
          before: "<script>", 
          after: "&lt;script&gt;" 
        }, {
          before: "&$/'", 
          after: "&amp;$&#x2F;&#x27;"
        }, {
          before: "$_dont_touch_me", 
          after: "$_dont_touch_me" 
        }, {
          before: String.raw`\\\";alert('XSS');//`,
          after: "\\\\\\&quot;;alert(&#x27;XSS&#x27;);&#x2F;&#x2F;" 
        }
      ]
      naughtyStrings.forEach((string) => {
        expect(sanitizeInput(string.before)).toBe(string.after);
      });
    });

  });

});
